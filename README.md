# Implementation Engine - README

This README contains steps that are necessary to get your application up and running.

### What is this repository for? ###

* This repo contains code for lendin Core Engine
* Version: 0.0.1

### How do I get set up? ###

* Prerequisite: Java 8, PostgreSQL 10.5, MySql 5.7, maven, Tomcat8
	* Refer to this link for setup (https://docs.google.com/document/d/1xKH-MuakEw259kq24QvluB-VMg1YkHnX43cpWLCU1v0/edit?usp=sharing)
* Set up settings.xml in local maven repository.
    * For ubuntu, create this file in /home/.m2
```
        <?xml version="1.0" encoding="UTF-8"?>
        <settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
           <servers>
              <server>
                  <username>manc</username>
                  <password>AP65MLgt6hmJya7iS6uwqMGDnne</password>
                  <id>lendin-central</id>
              </server>
           </servers>
           <profiles>
              <profile>
                 <repositories>
                    <!-- <repository>
                       <id>central</id>
                       <name>Central Repository</name>
                       <url>https://repo.maven.apache.org/maven2</url>
                       <layout>default</layout>
                       <snapshots>
                          <enabled>false</enabled>
                       </snapshots>
                    </repository> -->
                    <repository>
                       <releases>
                          <enabled>true</enabled>
                       </releases>
                       <id>lendin-central</id>
                       <name>lendin-artifactory-releases</name>
                       <url>http://artifactory.getlend.in/artifactory/lend-in-jars</url>
                    </repository>
                 </repositories>
                 <pluginRepositories>
                    <pluginRepository>
                       <id>central</id>
                       <name>Central Repository</name>
                       <url>https://repo.maven.apache.org/maven2</url>
                       <layout>default</layout>
                       <snapshots>
                          <enabled>false</enabled>
                       </snapshots>
                       <releases>
                          <updatePolicy>never</updatePolicy>
                       </releases>
                    </pluginRepository>
                 </pluginRepositories>
                 <id>artifactory</id>
              </profile>
           </profiles>
           <activeProfiles>
              <activeProfile>artifactory</activeProfile>
           </activeProfiles>
        </settings>
```
* How to run tests: # TODO
* Deployment instructions: # TODO

### Contribution guidelines ###

**Code guidelines:**
	
*	Create your fork from "https://bitbucket.org/kulizadev/implementation_engine/src/master/"
*	Do clone of forked repo and move to the cloned directory
*	Add upstream command: "git remote add upstream https://<your bitbucket id>@bitbucket.org/kulizadev/implementation_engine.git"
*	You can start coding...
*	For pushing code to main repo, you'll need to create a pull request from your fork to main repo
*	For syncing code with main repo: "git pull upstream <required branch>"
*	Setting up google code style in IDE
		
**Installing the coding style settings in Intellij**  

		Download the intellij-java-google-style.xml file from the http://code.google.com/p/google-styleguide/repo.
		
**Windows**

		Copy it into your config/codestyles folder in your IntelliJ settings folder. Under Settings -> Editor -> Code Style select the google-styleguide as current code style for the Metanome project.
		
**Macintosh**

		Download it and go into Preferences -> Java -> Editor -> Code Style. Click on Manage and import the downloaded Style Setting file. Select GoogleStyle as new coding style.
		
**Installing the coding style settings in Eclipse**

		Download the eclipse-java-google-style.xml file from the http://code.google.com/p/google-styleguide/ repo. Under Window/Preferences select Java/Code Style/Formatter. Import the settings file by selecting Import.
		
* Writing tests: # TODO
* Code review: # TODO

                                                                     
# Steps for running application #

**Setup database (Postgresql):**

*	Create db: createdb dbName
*	Create user(with password): createuser -P -s -e dbName_user

***

**Setting environment constants:**

* Add following to: vim ~/.bash_profile  
    *	export DATABASE_PLATFORM='postgres'  
	*	export DATABASE_URL='jdbc:postgresql://localhost:5432/db_name?characterEncoding=UTF-8'	(Postgres default port number is 5432, localhost can be replaced by IP)  
	*	export DATABASE_USER = 'your postgres username'  
	*	export DATABASE_PASSWORD = 'your postgres password'	  
	*	export SPRING_JPA_PROPERTIES_HIBERNATE_DIALECT = 'org.hibernate.dialect.PostgreSQLDialect'
	*   export DATABASE_DRIVER = 'org.postgresql.Driver'  

***

**Setup database (MySQL):**

*	Create db: create database dbName

***

**Setting environment constants:**

* Add following to: vim ~/.bash_profile  
    *	export DATABASE_PLATFORM='mysql'  
	*	export DATABASE_URL='jdbc:mysql://localhost:3306/db_name?characterEncoding=UTF-8'	(MySQL default port number is 3306, localhost can be replaced by IP)  
	*	export DATABASE_USER = 'your mysql username'  
	*	export DATABASE_PASSWORD = 'your mysql password'	  
	*	export SPRING_JPA_PROPERTIES_HIBERNATE_DIALECT = 'com.kuliza.lending.common.connection.CustomMySQLDialect'
	*   export DATABASE_DRIVER = 'com.mysql.jdbc.Driver'  

* Activate env variables: source ~/.bash_profile

***

**Run workflow wars from artifactory**

* Download the following wars from artifactory(http://artifactory.getlend.in/artifactory/webapp/#/artifacts/browse/tree/General/product) using your credentials
    * wf-pcp.war
    * wf-task.war
    * wf-iam.war
    
**Flowable Database and IDM properties for wf-pcp.war**

__IDM__

*   flowable.common.app.idm-url=${IDM_APP_URL:http://localhost:8091/wf-iam}
*   flowable.common.app.idm-admin.user=${IDM_APP_USER:administrator}
*   flowable.common.app.idm-admin.password=${IDM_APP_PASSWORD:Kuliza@123}
*   flowable.common.app.idm-redirect-url=${IDM_APP_URL:http://localhost:8091/wf-iam}
*   flowable.common.app.redirect-on-auth-success=${IDM_APP_AUTH_SUCCESS:http://localhost:8090/wf-pcp}
*   flowable.modeler.app.deployment-api-url=${APP_DEPLOYMENT_URL:http://localhost:8080/wf-task/app-api}

__DATABASE__

*   spring.datasource.driver-class-name=${DATABASE_DRIVER:com.mysql.jdbc.Driver}
*   spring.datasource.url=${DATABASE_URL:jdbc:mysql://127.0.0.1:3306/training?characterEncoding=UTF-8}
*   spring.jpa.properties.hibernate.dialect = ${DATABASE_DIALECT:org.hibernate.dialect.MySQL5Dialect}
*   spring.datasource.username=${DATABASE_USER:root}
*   spring.datasource.password=${DATABASE_PASSWORD:mysql}

##

    
*   Run these workflow wars in tomcat.
*    If you have an existing maven repository, then 
    * Either purge it using following command:
    __mvn dependency:purge-local-repository -Dinclude=com.kuliza, com.lendin__
    * If there is any error for checksum mismatch in ACT_CO_DATABASECHANGELOG, then clear the MD5SUM in ACT_CO_DATABASECHANGELOG table using following command and rerun.
    __update ACT_CO_DATABASECHANGELOG set MD5SUM=null;__
            
    * OR
        * Go to the local maven repository. (For ubuntu, its /home/.m2)
        * Delete all the jars present in /org/flowable folder

***

**Running Springboot Application:**

To clean your project..  

__mvn clean install__ (this command will run all the test cases)  
__mvn clean install -Dmaven.test.skip=true__ (this command will skip all the test cases)
	
To run the application  
__mvn spring-boot:run__ (this command will run the module)

* Swagger UI link: "http://dev-los.getlend.in:8080/journey-0.0.1-SNAPSHOT/swagger-ui.html#/" 

***

**Changes for running application using mysql(5.7.27)**

Setup database:

*	Create db: create database db_name;

***

**Changes in lend-in-core-engine/pom.xml**
	
	<dependency>
		<groupId>mysql</groupId>
	    <artifactId>mysql-connector-java</artifactId>
	</dependency>

***

**Modules to be imported from core product.**

Journey: This module contains all the APIs for running a journey/workflow, documents, mpin, generate password, OTP. (Import if you want Journey Core Engine)
	
	<dependency>
			<groupId>com.kuliza.lending</groupId>
			<artifactId>journey</artifactId>
			<version>${project.version}</version>
	</dependency>

***
Portal: This module contains all the APIs for running a portal/backoffice. (Import if you want Portal Core Engine)
	
	<dependency>
			<groupId>com.kuliza.lending</groupId>
			<artifactId>portal</artifactId>
			<version>${project.version}</version>
	</dependency>

*__Note:__ Here ${project.version} is a place holder and it eeds to be replaced by the release version of the product.*

***


## CUSTOM HOOKS

When the journey's dependency is successfully added in pom.xml, then we could access the APIs provided by core product. To make any modifications in these APIs we have given hooks. The customizable logics are listed below:

1) Authorization Hooks (src/main/java/com/kuliza/workbench/hook/AuthCustomService.java)

*	__Generate Password__: if the business has its own OTP partner then this method should be overrided.
*	__Validate Password__: if the business has its own OTP partner then this method should be overrided.
*	__Send password__: This method has to be always overrided if you want to send password to the user as there is no default provided by the product.

2) DMS Hooks (src/main/java/com/kuliza/workbench/hook/DMSCustomService.java)

*	__Upload File__: if the business has a DMS partner to upload file then this method should be overrided.
*	__Download File__: if the business has a DMS partner to download file then this method should be overrided.

3) Workflow Hooks (src/main/java/com/kuliza/workbench/hook/WorkflowCustomService.java)

*	__Pre-Initiate Script__: If any custom logic is required before initiating a workflow this method should be overrided.
*	__Post-Initiate Script__: If any custom logic is required after initiating a workflow this method should be overrided.
*	__Pre-Submit Script__: If any custom logic is to be written before submitting a form then this method should be triggered.
*	__Post-Submit Script__: If any custom logic is to be written after submitting a form then this method should be overrided.

4) Product Callback Hooks (src/main/java/com/kuliza/workbench/hook/ProductCallbackCustomService.java)

*	__Execute__: If any custom logic is required in the Callback API. It has to go here.

5) Validation Hook

*	__Add Stratergies__: If any new strategies has to be added then create a new implementation of ValidationStratergy class with custom validation logic. This will get executed at runtime along with in-built validations.


To activate these hooks add these to application.preoperties file if it's not there already

*	workflow.hook.type=${WORKFLOW_HOOK_TYPE:workflowCustomService}
*	dms.hook.type=${DMS_HOOK_TYPE:dmsCustomService}
*	auth.hook.type=${AUTH_HOOK_TYPE:authCustomService}
*   callback.hook.type=${PRODUCT_CALLBACK_HOOK_TYPE:productCallbackCustomService}

(If none of the properties are mentioned then product's default logic will run.)

For more details refer to lend-in-core-engine/modules/journey/src/main/java/com/kuliza/lending/journey/contract.

***
***



## Properties Config ##

Every module contains these two properties file

*	application.properties
*	lend-in-modules.properties

**In some modules there might be some other properties file like**

*	application-dev.properties (It contains properties related to dev environment)
*	application-uat.properties (It contains properties related to testing environment)
*	application-sales.properties (It contains properties related to sales environment)
	
We don't make any change in application.properties, until and unless the changes are that module specific. The changes are done in lend-in-modules.properties file.
	
**Add following to: vim ~/.bash_profile**
	
	################## Database Properties #############

		export DATABASE_PLATFORM='<your database (postgres/mysql)>'
		export DATABASE_URL='jdbc:mysql://localhost:3306/db_name?characterEncoding=UTF-8'	(Mysql default port number is 3306, localhost can be replaced by IP)
		export DATABASE_USER='<your database username>'
		export DATABASE_PASSWORD='<your database password>'
		export SPRING_JPA_PROPERTIES_HIBERNATE_DIALECT='com.kuliza.lending.common.connection.CustomMySQLDialect' (Write dialect according to your database, default MySQL)
        export DATABASE_DRIVER='com.mysql.jdbc.Driver' (Write your own driver, default is MySQL)

	################## Keycloak Properties #############
	
	## You can change the values of the following variables according to your server details.
		export IAM_HOST='http://35.200.188.121:8005/'
		export IAM_CLIENT_ID='lending'	
		export IAM_CLIENT_SECRET='a827f002-fd8e-42e1-b374-7f9541358b44'	
		export IAM_REALM='kuliza_ce'	
		export IAM_ADMIN_USERNAME='admin@kuliza.com'
		export IAM_ADMIN_EMAIL='admin@kuliza.com'
		export IAM_ADMIN_PASSWORD='test'

	################## PORTAL Server settings ################
	
	## You can change the values of the following variables according to your server details.
		export JOURNEY_PROTOCOL='http'
		export JOURNEY_HOST='dev-los.getlend.in'
		export JOURNEY_PORT=8000
		export JOURNEY_SUB_URL='/journey-0.0.1-SNAPSHOT'

	################## PORTAL Server settings ################
	
	## You can change the values of the following variables according to your server details.
		export PORTAL_PROTOCOL='http'
		export PORTAL_HOST='dev-los.getlend.in'
		export PORTAL_PORT=8000
		export PORTALSUB_URL='/journey-0.0.1-SNAPSHOT'
		export PORTAL_TIMEOUT=90000

	################## MASTERS Server settings ################
	
	## You can change the values of the following variables according to your server details.
		export MASTER_PROTOCOL='http'
		export MASTER_HOST='dev-los.getlend.in'
		export MASTER_PORT=8080
		export MASTER_SUB_URL='/masters-0.0.1-SNAPSHOT'
		export MASTER_TIMEOUT=90000
		
	################ IB Settings ############
	
	## You can change the values of the following variables according to your server details.
		export IB_PROTOCOL='http'
		export IB_HOST='dev-los.getlend.in'
		export IB_PORT=8080
		export IB_SUB_URL='/ib'
		export IB_TIMEOUT=90000

	################## CONFIGURATOR settings ################
	
	## You can change the values of the following variables according to your server details.
		export CONFIGURATOR_PROTOCOL='http'
		export CONFIGURATOR_HOST='dev-los.getlend.in'
		export CONFIGURATOR_PORT=8000
		export CONFIGURATOR_SUB_URL='/configurator-0..0.1-SNAPSHOT'
		export CONFIGURATOR_TIMEOUT=90000


	################## Notification Server settings ################
	
	## You can change the values of the following variables according to your server details.
		export IB_PROTOCOL='http'
		export IB_HOST='dev-los.getlend.in'
		export IB_PORT=8080
		export IB_SUB_URL='/ib'

	############## DMS Configuration properties ###########
	
	## You can change the values of the following variables according to your server details.
		export DMS_HOST='35.200.188.121'
		export DMS_PORT=8000
		export DMS_PROTOCOL='http'
		export DMS_SUBURL='/api/dms/kdms/documents/'

		export SERVICE_NAME_TO_HOSTS={DMS:'35.200.188.121:8000',MASTERS:'dev-los.getlend.in:8080'}
		export PORTAL_SERVICE_NAME_TO_HOSTS={DMS:'35.200.188.121:8000',MASTERS:'dev-los.getlend.in:8080'}
    
    ############## LendIn  properties for IB integration and AES encryption  ###########

    ## You can change the values of the following variables according to your server details.
            
        export LENDIN_IB_PUBLIC_KEY='thisisasecretkey'
        export LENDIN_IB_INIT_VECTOR=0000000000000000
        export LENDIN_IB_HOST_URL=http://ib-backend.getlend.in/ib/api/external-integration
        export LENDIN_IB_COMPANY_SLUG=kuliza
        export LENDIN_IB_COMPANY_ID=5
        export LENDIN_AES_PRIVATE_KEY=VmYq3t6w9z$C&F)J
        export LENDIN_MASTERS_HOST_URL=http://dev-los.getlend.in:8080/masters-0.0.1-SNAPSHOT

***
***

**Note:**

Following property should be set to false in production environments
```
spring.resources.add-mappings=${RESOURCE_MAPPINGS:true}
```

In case, you are using MySQL and the default charset for any table is anything other than utf-8, then execute following commands to change it to utf-8

```
1. ALTER DATABASE dbName CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
2. ALTER TABLE tableName CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
```

It is important for masters, because data storage often contains unicode string which won't be supported completely by other charsets.
