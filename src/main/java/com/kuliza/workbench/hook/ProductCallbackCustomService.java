package com.kuliza.workbench.hook;

import com.kuliza.lending.engine_common.base.ProductCallbackDefaultService;
import com.kuliza.lending.engine_common.pojo.ProductCallbackRequest;
import javax.servlet.ServletRequest;
import org.springframework.stereotype.Service;

@Service("productCallbackCustomService")
public class ProductCallbackCustomService extends ProductCallbackDefaultService {

  @Override
  public void execute(
      String username, ServletRequest request, ProductCallbackRequest callbackRequest) {
    // Custom code for callback will come here
  }
}
